const express = require('express');
const auth = require('../middleware/auth');

const Comment = require('../models/Comment');

const router = express.Router();

router.post('/', auth,(req, res) => {
    const commentData = req.body;
    commentData.user = req.user._id;

    const comment = new Comment(commentData);

    comment.save(comment)
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});




module.exports = router;