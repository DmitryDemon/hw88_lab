const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');

const Post = require('../models/Post');
const Comment = require('../models/Comment');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    Post.find().populate('user').sort({datetime: -1})
        .then(posts=> res.send(posts))
        .catch(()=>res.sendStatus(500))
});

router.get('/:id', async (req, res) => {
    try {
        const comments = await Comment.find({post:req.params.id}).populate('user').sort('_id');
        const post = await Post.findById(req.params.id);
        res.send({post, comments});
    }catch (e) {
        res.sendStatus(500);
    }
});


router.post('/',auth, upload.single('image'), (req, res) => {
    const postData = req.body;
    postData.user = req.user._id;
    postData.datetime = new Date().toISOString();
    if  (!(postData.description || postData.image)){
        return res.send.status(400)
    }
    if (req.file) {
        postData.image = req.file.filename;
    }

    const post = new Post(postData);

    post.save(post)
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))

});



module.exports = router;