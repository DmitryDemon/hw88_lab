const express = require('express');

const User = require('../models/User');
const auth = require('../middleware/auth');

const router = express.Router();

router.post('/', async (req, res) => {
    const user = new User(req.body);

    user.generateToken();

    try {
        await user.save();
        return res.send({message: 'User registered', user});
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.post('/sessions', async (req, res) => {

    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(400).send({error: 'Username not found'});
    }
    const isMatch = await user.checkPassword(req.body.password);
    console.log("is math", isMatch);

    if (!isMatch) {
        return res.status(400).send({error: 'Password is wrong'});
    }

    user.generateToken();

    await user.save();

    res.send({message: 'Login successful', user});
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');

    const success = {message:'Logged out'};

    if (!token){
        return res.send(success)
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success)
    }

    user.generateToken();
    await user.save();

    return res.send(success)


});

router.put('/',auth, async (req,res) => {


    req.user.password = req.body.password;

    await req.user.save();

    res.sendStatus(200);
});


router.get('/', (req, res) => {
    User.find()
        .then(users=> res.send(users))
        .catch(()=>res.sendStatus(500))
});

module.exports = router;
