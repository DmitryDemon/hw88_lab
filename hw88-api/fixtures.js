const mongoose = require('mongoose');
const config = require('./config');

const Post = require('./models/Post');
const User = require('./models/User');
const Comment = require('./models/Comment');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            username: 'demon',
            password: '1',
            token: '1',
        },
        {
            username: 'stary',
            password: '2',
            token: '2',
        },
        {
            username: 'coder',
            password: '3',
            token: '3',
        },
    );

    const post = await Post.create(
        {
            user: user[2]._id,
            title: 'for newbies',
            image:'nub.jpg',
            description: 'Страйкбо́л (от англ. strike — удар, ball — шар) — командная, военно-тактическая игра.' +
                'Российский аналог командной игры, более известной как эйрсофт (англ. airsoft) с ' +
                'использованием «мягкой пневматики» (разрешенная дульная энергия в России не более 3 Дж),' +
                ' но в страйкболе чаще всего энергия 1,7 Дж, использующей пластиковые шарики калибром 6 мм (изредка 8 мм)',
            datetime: '21.03.2019'
        },
        {
            user: user[2]._id,
            title: 'trade',
            image:'trade.jpeg',
            description: 'Запрещается создавать темы, где предмет сделки не относится ' +
                'к Страйкболу или страйкбольному снаряжению (огнестрельное оружие, автомобили, оргтехника и прочее). ',
            datetime: '11.04.2019'
        },
        {
            user: user[1]._id,
            title: 'games',
            image:'games.jpeg',
            description: 'В этом разделе ононсируются игры и отписываются гроки о своем присутствии на них',
            datetime: '12.04.2019'
        },
    );


    await Comment.create(
        {
            post: post[0]._id,
            user: user[0]._id,
            text: 'Привед всем меня зовуд Дима и я нубяра',
        },
        {
            post: post[0]._id,
            user: user[1]._id,
            text: 'Привед Дима,меня зовут СТАРЫЙ,добро пожаловать в страйкбол',
        },
        {
            post: post[0]._id,
            user: user[2]._id,
            text: 'Ку нубасы , всем читать правила иначе не допущу к игре',
        },
        {
            post: post[1]._id,
            user: user[0]._id,
            text: 'Куплю снарягу(всю),предлогайте предложения,бюджет 15к',
        },
        {
            post: post[1]._id,
            user: user[2]._id,
            text: 'Как же задолбали эти новички..Это так не работает.Ты сам должен найти продовца и выбрать снарягу',
        },
        {
            post: post[1]._id,
            user: user[1]._id,
            text: 'demon у тебя бюджет маловат,норм привод стоит минимум 15к так что либо копи либо ' +
                'придется брать лютое говно..не советую',
        },
        {
            post: post[2]._id,
            user: user[2]._id,
            text: '27.04.19 состоится игра на полигоне Солнечный ' +
                'всем жилающим просьба отписаться в теме',
        },
        {
            post: post[2]._id,
            user: user[0]._id,
            text: 'А мне можно приехать просто посмотреть,просто снарягу еще не купил',
        },
        {
            post: post[2]._id,
            user: user[2]._id,
            text: 'попробуй',
        },
        {
            post: post[2]._id,
            user: user[1]._id,
            text: '+1',
        },
);


    await connection.close();
};



run().catch(error => {
    console.log('Something went wrong', error);
});