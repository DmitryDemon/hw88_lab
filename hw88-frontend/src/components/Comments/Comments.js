import React, {Fragment} from 'react';
import {Card, CardBody, CardText, CardTitle} from "reactstrap";
import PropTypes from "prop-types";

const Comments = props => {
  return (
    <Fragment>
      <Card style={{marginBottom: '10px'}}>
        <CardBody>
          <CardTitle>{props.user}</CardTitle>
          <CardText>{props.text}</CardText>
        </CardBody>
      </Card>
    </Fragment>
  );
};

Comments.propTypes ={
  user: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Comments;