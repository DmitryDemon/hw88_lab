import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";
import {Link} from "react-router-dom";

import PostThumbnail from "../PostThumbnail/PostThumbnail";

const PostListItem = props => {
  return (
    <Card style={{marginBottom: '10px'}}>
      <CardBody>
        <PostThumbnail image={props.image}/>
        <p style={{position: 'absolute', top: '15px', left: '150px'}}>{props.datetime} by <strong>{props.user}</strong></p>
        <Link to={`/posts/${props._id}`} style={{position: 'absolute', top: '70px', left: '150px'}}>
          <h2>{props.title}</h2>
        </Link>
      </CardBody>
    </Card>
  );
};

PostListItem.propTypes = {
  image: PropTypes.string,
  _id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  datetime: PropTypes.string.isRequired
};

export default PostListItem;
