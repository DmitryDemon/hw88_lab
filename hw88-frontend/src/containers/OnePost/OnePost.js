import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardImg, CardText, CardTitle, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {apiURL} from "../../constants";
import {connect} from "react-redux";
import {fetchOnePost} from "../../store/actions/onePostActions";
import Comments from "../../components/Comments/Comments";
import {createComment} from "../../store/actions/commentsActions";

class OnePost extends Component {
  state = {
    text: '',
    post: this.props.match.params.id
  };

  componentDidMount() {
    this.props.onFetchPost(this.props.match.params.id);
  }

  submitFormHandler = event => {
    event.preventDefault();

    this.props.onCommentCreated({...this.state}).then(
      () => this.setState({text: ''})
    );
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  render() {
    return (
      <Fragment>
        {this.props.onePost ?
            <Card style={{width: '640px', marginBottom: '25px'}} >
              <CardImg top style={{width: "100%"}} src={apiURL + '/uploads/' + this.props.onePost.post.image} alt="post" />
              <CardBody>
                <CardTitle>{this.props.onePost.post.title}</CardTitle>
                <CardText>{this.props.onePost.post.description}</CardText>
                <CardText>{this.props.onePost.post.datetime}</CardText>
              </CardBody>
            </Card>
        : null}
        
        {this.props.onePost && this.props.onePost.comments.map(comment => (
            <Comments key={comment._id} user={comment.user.username} text={comment.text}/>
        ))}
        {this.props.user ?
          <Form onSubmit={this.submitFormHandler}>
            <FormGroup row>
              <Label sm={2} for="text">Comment</Label>
              <Col sm={10}>
                <Input
                    type="textarea" required
                    name="text" id="text"
                    placeholder="Enter comment"
                    value={this.state.text}
                    onChange={this.inputChangeHandler}
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Col sm={{offset: 2, size: 10}}>
                <Button type="submit" color="primary">Send</Button>
              </Col>
            </FormGroup>
          </Form>:null}


      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  onePost: state.posts.onePost,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchPost: (posts) => dispatch(fetchOnePost(posts)),
  onCommentCreated: (commentData) => dispatch(createComment(commentData))
});

export default connect(mapStateToProps, mapDispatchToProps)(OnePost);
