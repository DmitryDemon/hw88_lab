import React, {Component, Fragment} from 'react';
import {fetchPosts} from "../../store/actions/postsActions";
import {connect} from "react-redux";
import PostListItem from "../../components/PostListItem/PostListItem";

class Posts extends Component {
  componentDidMount() {
    this.props.onFetchPosts();
  }

  render() {
    return (
      <Fragment>
        <h2>
          Posts
        </h2>
        {this.props.posts.map(post => (
          <PostListItem
            key={post._id}
            _id={post._id}
            title={post.title}
            datetime={post.datetime}
            user={post.user.username}
            image={post.image}
          />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts
});

const mapDispatchToProps = dispatch => ({
  onFetchPosts: () => dispatch(fetchPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
