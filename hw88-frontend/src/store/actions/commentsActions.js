import axios from '../../axios-api';
import {fetchOnePost} from "./onePostActions";

export const CREATE_COMMENTS_SUCCESS = 'CREATE_COMMENTS_SUCCESS';

export const createCommentsSuccess = comment => ({type: CREATE_COMMENTS_SUCCESS, comment});

export const  createComment = commentData => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    return axios.post('/comments', commentData, {headers: {'Authorisation': token}}).then(
      () => {
        dispatch(createCommentsSuccess());
        dispatch(fetchOnePost(commentData.post))
      }
    );
  };
};
