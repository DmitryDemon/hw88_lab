import axios from '../../axios-api';

export const FETCH_ONE_POST_SUCCESS = 'FETCH_ONE_POST_SUCCESS';

export const fetchOnePostsSuccess = posts => ({type: FETCH_ONE_POST_SUCCESS, posts});

export const fetchOnePost = id => {
  return (dispatch) => {
    return axios.get('/posts/' + id).then(
      response => {
        dispatch(fetchOnePostsSuccess(response.data));
      }
    );
  };
};