import axios from '../../axios-api';

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';

export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});

export const fetchPosts = () => {
  return dispatch => {
    return axios.get('/posts').then(
      response => dispatch(fetchPostsSuccess(response.data))
    );
  };
};

export const createPost = postData => {

  return (dispatch,getState) => {
    const token = getState().users.user.token;
    const postId = postData.post;
    return axios.post('/posts', postData,{headers:{'Authorisation':token}}).then(
      () => dispatch(createPostSuccess(postId))
    );
  };
};
