import {FETCH_POSTS_SUCCESS} from "../actions/postsActions";
import {FETCH_ONE_POST_SUCCESS} from '../actions/onePostActions';

const initialState = {
  posts: [],
  onePost: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_SUCCESS:
      return {...state, posts: action.posts};
    case FETCH_ONE_POST_SUCCESS:
      return {...state, onePost: action.posts};
    default:
      return state;
  }
};

export default reducer;
